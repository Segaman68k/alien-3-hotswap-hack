@echo off

cls

set AS_MSGPATH=bin/msg
set USEANSI=n

bin\asw  -xx -c -A Alien3.asm
IF NOT EXIST Alien3.p goto LABLPAUSE
IF EXIST Alien3.p goto Convert2ROM
:Convert2ROM
bin\p2bin Alien3.p out\Alien3.bin -l 0 -r $-$
bin\rompad.exe out\Alien3.bin 255 0
bin\fixheadr.exe out\Alien3.bin
del Alien3.p
del Alien3.h
exit /b
:LABLPAUSE

pause


exit /b
