; ---------------------------------------------------------------------------
; Configure compilator AS
; ---------------------------------------------------------------------------

	CPU 68000
	padding off ; we don"t want AS padding out dc.b instructions
	listing off ; we don"t need to generate anything for a listing file
	supmode on  ; we don"t need warnings about privileged instructions

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; 			Build configuraton
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

modlistcount set 0

modSnapshot 	= 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ---------------------------------------------------------------------------
; Insert ROM Image
; ---------------------------------------------------------------------------
	org	$0
	binclude	"Alien 3 (UE) (REV01) [!].bin"

; ---------------------------------------------------------------------------
; Header
; ---------------------------------------------------------------------------
	org $120
	dc.b "ALIEN 3 HOTSWAPGLITCH HACK BY SEGAMAN           "
	dc.b "ALIEN 3 HOTSWAPGLITCH HACK BY SEGAMAN           "
	org $1F0
	dc.b "EUJ"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

endofrom set 1

	org     $80000
	
 ;if * < $80000
 ;error "too low address : $\{(*)}"
 ;endif
	
; ---------------------------------------------------------------------------
; Include mods in End of ROM Space
; ---------------------------------------------------------------------------
	include ModList.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

endofrom set 0  ; we're not at the end of rom

; ---------------------------------------------------------------------------
; Include mods in ROM Space
; ---------------------------------------------------------------------------
	include ModList.asm

; ---------------------------------------------------------------------------
; Errors
; ---------------------------------------------------------------------------
 if modlistcount <> 2
 error "ModList inserted less than 2 times : $\{(modlistcount)}"
 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
