# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is hack for Alien 3 for Sega Genesis.

It adds memory snapshot, like it works when you hotswap cart from another to alien 3.

This effect creates a lot of glitches with a lot of numbers.

You can start with random amount of ammo, life and even time.

This hack fixes:

* Issues with real hardware console
* Fixes bug, when you cannot shoot even if you have a lot of ammo
* Fixes bug with end level counting score

Checkout video for more information (warning: russian only)

https://youtu.be/nZDoVS8maXw

### How do I get set up? ###

* Clone repo
* Start "compile.bat"
* "out" folder will hold result

### Contribution guidelines ###

Hack begins inside "Alien3.asm"

It works by including "ModList.asm" 2 times:

* 1) for inside ROM patches (such as fixes or PC jumps)
* 2) outside ROM patches (such as code)

Inside "mods/Snapshot.asm" you can see more about building "in rom" and "out rom"

It triggers by " if endofrom = 0"

### Warning ###

Dont forget that every mod compiles twice in a row.

That means if you including items (such as Macros) include it insude "if endofrom = 0" <> "endif"

This will prevent macros to define twice.

### Who do I talk to? ###

* youtube.com/user/SegamanSoft
* twitch.tv/segaman8
* twitter.com/Segaman8
* patreon.com/Segaman
