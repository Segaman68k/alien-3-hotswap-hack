; ---------------------------------------------------------------------------
Debug:
                movem.l d0-a7,-(sp)
                bsr.s   loc_0_BD94
                move.w  d0,-(sp)
                movea.l sp,a6
                bsr.w   loc_0_C154
                movem.l $22(a6),d0-d7
                movem.l d0-d7,-(sp)
                movem.l 2(a6),d0-d7
                movem.l d0-d7,-(sp)
                move.w  (a6),d0
                add.w   d0,d0
                slea0     word_0_C5AE,a0
                adda.w  (a0,d0.w),a0
                move.l  a0,-(sp)
                lsr.w   #1,d0
                move.w  d0,-(sp)
                slea0     word_0_BEC0,a0
                move.l  #$C000,d3
                bsr.w   loc_0_BFF0
                movea.l a6,sp
                cmpi.w  #2,(a6)
                beq.s   loc_0_BDEC
                cmpi.w  #3,(a6)
                bne.s   loc_0_BE4A
                bra.s   loc_0_BDEC

loc_0_BD94:                             ; CODE XREF: ROM:0000BDA0p
                moveq   #0,d0
                move.b  (sp),d0
                clr.b   (sp)
                rts
loc_0_BDEC:                             ; CODE XREF: ROM:0000BDE4j
                move.w  $4A(a6),-(sp)
                move.l  $4C(a6),-(sp)
                slea0     ScreenElements,a0
                bsr.w   loc_0_BFF0
                movea.l a6,sp
                move.w  $48(a6),-(sp)
                move.b  $43(a6),d0
                andi.w  #7,d0
                add.w   d0,d0
                slea0     Text_C55A,a0
                adda.w  (a0,d0.w),a0
                move.l  a0,-(sp)
                slea0     t_instr,a0      ; "Instr"
                btst    #3,$43(a6)
                beq.s   loc_0_BE26
                slea0     t_not,a0        ; "Not "

loc_0_BE26:                             ; CODE XREF: ROM:0000BE20j
                move.l  a0,-(sp)
                slea0     t_write,a0      ; "Write"
                btst    #4,$43(a6)
                beq.s   loc_0_BE38
                slea0     t_read,a0       ; "Read"

loc_0_BE38:                             ; CODE XREF: ROM:0000BE32j
                move.l  a0,-(sp)
                move.l  $44(a6),-(sp)
                slea0     t_access,a0
                bsr.w   loc_0_BFF0
                movea.l a6,sp
                bra.s   loc_0_BE5C
; ---------------------------------------------------------------------------

loc_0_BE4A:                             ; CODE XREF: ROM:0000BDEAj
                move.w  $42(a6),-(sp)
                move.l  $44(a6),-(sp)
                slea0     ScreenElements,a0
                bsr.w   loc_0_BFF0
                movea.l a6,sp

loc_0_BE5C:                             ; CODE XREF: ROM:0000BE48j
                move.l  a6,d0
                addi.l  #$42,d0 ; 'B'
                andi.l  #-$10,d0
                movea.l d0,a1
                moveq   #1,d1

loc_0_BE6E:                             ; CODE XREF: ROM:0000BEB0j
                move.l  a1,-(sp)
                slea0     word_0_BFC1,a0
                bsr.w   loc_0_BFF0
                movea.l a6,sp

loc_0_BE7A:                             ; CODE XREF: ROM:0000BEB2j
                move.l  $C(a1),-(sp)
                move.l  8(a1),-(sp)
                move.l  4(a1),-(sp)
                move.l  (a1),-(sp)
                move.w  a1,-(sp)
                slea0     word_0_BFC8,a0
                bsr.w   loc_0_BFF0
                movea.l a6,sp
                adda.l  #$10,a1
                cmpi.b  #$1B,d2
                bge.s   loc_0_BEB4
                move.l  a1,d0
                andi.l  #$FFFFFF,d0
                beq.s   loc_0_BEB4
                andi.l  #$FF,d0
                beq.s   loc_0_BE6E
                bra.s   loc_0_BE7A
; ---------------------------------------------------------------------------

loc_0_BEB4:                             ; CODE XREF: ROM:0000BE9Ej
                                        ; ROM:0000BEA8j
                movea.l a6,sp
                addq.l  #2,sp
                movem.l (sp)+,d0-a6
                addq.l  #4,sp

loc_0_BEBE:                             ; CODE XREF: ROM:loc_0_BEBEj
                bra.s   loc_0_BEBE
; ---------------------------------------------------------------------------
word_0_BEC0:    dc.w $F102, $2F3,$4578,$6365; 0 ; DATA XREF: ROM:0000BDD0o
                dc.w $7074,$696F,$6E20,$2425; 4
                dc.w $623A,$20F4,$2573,$F0F0; 8
                dc.w $F244,$303A,$2025,$6CF0; 12
                dc.w $4431,$3A20,$256C,$F044; 16
                dc.w $323A,$2025,$6CF0,$4433; 20
                dc.w $3A20,$256C,$F044,$343A; 24
                dc.w $2025,$6CF0,$4435,$3A20; 28
                dc.w $256C,$F044,$363A,$2025; 32
                dc.w $6CF0,$4437,$3A20,$256C; 36
                dc.w $F114, $441,$303A,$2025; 40
                dc.w $6CF0,$4131,$3A20,$256C; 44
                dc.w $F041,$323A,$2025,$6CF0; 48
                dc.w $4133,$3A20,$256C,$F041; 52
                dc.w $343A,$2025,$6CF0,$4135; 56
                dc.w $3A20,$256C,$F041,$363A; 60
                dc.w $2025,$6CF0,$5350,$3A20; 64
                dc.w $256C              ; 68
                dc.b   0
ScreenElements: dc.w $F102, $DF5,$5043,$3AF2; 0 ; DATA XREF: ROM:0000BDF4o
                                        ; ROM:0000BE52o
                dc.w $2025,$6C20,$2020,$2020; 4
                dc.w $20F5,$5352,$3AF2,$2025; 8
                dc.w $77F0,$F000        ; 12
t_access:       dc.w $4163,$6365,$7373,$2041; 0 ; DATA XREF: ROM:0000BE3Eo
                dc.w $6464,$7265,$7373,$3A20; 4
                dc.w $2020,$256C,$F041,$6363; 8
                dc.w $6573,$7320,$4D6F,$6465; 12
                dc.w $3A20,$2020,$2020,$2025; 16
                dc.w $732C,$2025,$73F0,$4164; 20
                dc.w $6472,$6573,$7320,$5370; 24
                dc.w $6163,$653A,$2020,$2020; 28
                dc.w $2573,$F049,$6E73,$7472; 32
                dc.w $7563,$7469,$6F6E,$2052; 36
                dc.w $6567,$3A20,$2025,$77F0; 40
                dc.w $F000              ; 44
word_0_BFC1:    dc.w $F425,$6C3A,$F2F0  ; 0 ; DATA XREF: ROM:0000BE70o
                dc.b   0
word_0_BFC8:    dc.w $F425,$623A,$F225,$6C20; 0 ; DATA XREF: ROM:0000BE8Ao
                dc.w $256C,$2025,$6C20,$256C; 4
                dc.w $F000              ; 8
t_read:         dc.b "Read",0           ; DATA XREF: ROM:0000BE34o
t_write:        dc.b "Write",0          ; DATA XREF: ROM:0000BE28o
t_not:          dc.b "Not "             ; DATA XREF: ROM:0000BE22o
t_instr:        dc.b "Instr",0          ; DATA XREF: ROM:0000BE16o
; ---------------------------------------------------------------------------
                align 2
loc_0_BFF0:                             ; CODE XREF: ROM:0000BDDAp
                                        ; ROM:0000BDF8p ...
                link    a6,#$FFFE
                move.b  d1,(sp)
                move.b  d2,1(sp)
                addq.l  #8,a6

loc_0_BFFC:                             ; CODE XREF: ROM:0000C01Aj
                                        ; ROM:0000C02Aj ...
                move.b  (a0)+,d0
                beq.w   loc_0_C092
                cmpi.b  #$25,d0 ; '%'
                bne.s   loc_0_C04A
                move.b  (a0)+,d0
                cmpi.b  #$73,d0 ; 's'
                bne.s   loc_0_C01E
                move.l  a0,-(sp)
                movea.l (a6)+,a0
                bsr.w   loc_0_BFF0
                movea.l (sp)+,a0
                bra.w   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C01E:                             ; CODE XREF: ROM:0000C00Ej
                cmpi.b  #$62,d0 ; 'b'
                bne.s   loc_0_C02E
                move.w  (a6)+,d0
                bsr.w   loc_0_C0C2
                bra.w   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C02E:                             ; CODE XREF: ROM:0000C022j
                cmpi.b  #$77,d0 ; 'w'
                bne.s   loc_0_C03C
                move.w  (a6)+,d0
                bsr.s   loc_0_C0B0
                bra.w   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C03C:                             ; CODE XREF: ROM:0000C032j
                cmpi.b  #$6C,d0 ; 'l'
                bne.s   loc_0_C04A
                move.l  (a6)+,d0
                bsr.s   loc_0_C09A
                bra.w   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C04A:                             ; CODE XREF: ROM:0000C006j
                                        ; ROM:0000C040j
                btst    #7,d0
                beq.s   loc_0_C08C
                cmpi.b  #-$F,d0
                bne.s   loc_0_C062
                move.b  (a0)+,d1
                move.b  (a0)+,d2
                move.b  d1,(sp)
                move.b  d2,1(sp)
                bra.s   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C062:                             ; CODE XREF: ROM:0000C054j
                cmpi.b  #-$10,d0
                bne.s   loc_0_C06E
                move.b  (sp),d1
                addq.b  #1,d2
                bra.s   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C06E:                             ; CODE XREF: ROM:0000C066j
                cmpi.b  #-$E,d0
                bcs.s   loc_0_C08C
                cmpi.b  #-$B,d0
                bhi.s   loc_0_C08C
                subi.b  #-$E,d0
                andi.b  #3,d0
                andi.w  #-8,d3
                or.b    d0,d3
                bra.w   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C08C:                             ; CODE XREF: ROM:0000C04Ej
                                        ; ROM:0000C072j ...
                bsr.s   loc_0_C0F8
                bra.w   loc_0_BFFC
; ---------------------------------------------------------------------------

loc_0_C092:                             ; CODE XREF: ROM:0000BFFEj
                movea.l sp,a6
                addq.l  #2,a6
                unlk    a6
                rts
; ---------------------------------------------------------------------------

loc_0_C09A:                             ; CODE XREF: ROM:0000C044p
                movem.l d0/d4,-(sp)
                moveq   #$10,d4
                rol.l   d4,d0
                bsr.s   loc_0_C0B0
                moveq   #$10,d4
                rol.l   d4,d0
                bsr.s   loc_0_C0B0
                movem.l (sp)+,d0/d4
                rts
; ---------------------------------------------------------------------------

loc_0_C0B0:                             ; CODE XREF: ROM:0000C036p
                                        ; ROM:0000C0A2p ...
                movem.l d0,-(sp)
                rol.w   #8,d0
                bsr.s   loc_0_C0C2
                rol.w   #8,d0
                bsr.s   loc_0_C0C2
                movem.l (sp)+,d0
                rts
; ---------------------------------------------------------------------------

loc_0_C0C2:                             ; CODE XREF: ROM:0000C026p
                                        ; ROM:0000C0B6p ...
                movem.l d0,-(sp)
                rol.b   #4,d0
                bsr.s   loc_0_C0D4
                rol.b   #4,d0
                bsr.s   loc_0_C0D4
                movem.l (sp)+,d0
                rts
; ---------------------------------------------------------------------------

loc_0_C0D4:                             ; CODE XREF: ROM:0000C0C8p
                                        ; ROM:0000C0CCp
                movem.l d0,-(sp)
                andi.w  #$F,d0
                move.b  letters(pc,d0.w),d0
                bsr.s   loc_0_C0F8
                movem.l (sp)+,d0
                rts
; ---------------------------------------------------------------------------
letters:        dc.b "0123456789ABCDEF"
; ---------------------------------------------------------------------------

loc_0_C0F8:                             ; CODE XREF: ROM:loc_0_C08Cp
                                        ; ROM:0000C0E0p
                movem.l d0/d2,-(sp)
                move.w  d3,-(sp)
                subi.b  #$20,d0 ; ' '
                bmi.s   loc_0_C10A
                cmpi.b  #$5F,d0 ; '_'
                blt.s   loc_0_C10E

loc_0_C10A:                             ; CODE XREF: ROM:0000C102j
                move.b  #$1F,d0

loc_0_C10E:                             ; CODE XREF: ROM:0000C108j
                andi.l  #$FF,d2
                lsl.l   #6,d2
                andi.l  #$FF,d1
                add.w   d1,d2
                add.w   d2,d2
                andi.w  #-8,d3
                add.w   d3,d2
                lsl.l   #2,d2
                lsr.w   #2,d2
                ori.w   #$4000,d2
                swap    d2
                move.l  d2,($C00004).l
                andi.w  #$FF,d0
                move.w  (sp),d3
                andi.w  #3,d3
                ror.w   #3,d3
                or.w    d3,d0
                move.w  d0,($C00000).l
                addq.w  #1,d1
                move.w  (sp)+,d3
                movem.l (sp)+,d0/d2
                rts
; ---------------------------------------------------------------------------

loc_0_C154:                             ; CODE XREF: ROM:0000BDA6p
                bsr.s   loc_0_C160
                bsr.w   set_colors
                bsr.w   loc_0_C22E
                rts
; ---------------------------------------------------------------------------

loc_0_C160:                             ; CODE XREF: ROM:loc_0_C154p
                bsr.s   Debug_VDP_Setup
                bsr.s   loc_0_C1A6
                rts
; ---------------------------------------------------------------------------

Debug_VDP_Setup:                        ; CODE XREF: ROM:loc_0_C160p
                                        ; ROM:0000C170j
                move.w  ($C00004).l,d0
                btst    #1,d0
                bne.s   Debug_VDP_Setup
                moveq   #$12,d0
                slea0     instr,a0
                lea     ($C00004).l,a1
                move.w  #-$8000,d1

loc_0_C182:                             ; CODE XREF: ROM:0000C18Cj
                move.b  (a0)+,d1
                move.w  d1,(a1)
                clr.b   d1
                addi.w  #$100,d1
                dbf     d0,loc_0_C182
                rts
; ---------------------------------------------------------------------------
instr:          dc.b   4,$44,$30,$1E    ; 0 ; DATA XREF: ROM:0000C174o
                dc.b   7,$3E,  0,  0    ; 4
                dc.b   0,  0,  0,  0    ; 8
                dc.b $81,$3F,  0,  2    ; 12
                dc.b   1,  0,  0,  0    ; 16
; ---------------------------------------------------------------------------

loc_0_C1A6:                             ; CODE XREF: ROM:0000C162p
                moveq   #0,d1
                lea     ($C00004).l,a0
                lea     ($C00000).l,a1
                move.l  #$40000000,(a0)
                move.w  #$3FFF,d0

loc_0_C1BE:                             ; CODE XREF: ROM:0000C1C0j
                move.l  d1,(a1)
                dbf     d0,loc_0_C1BE
                move.l  #-$40000000,(a0)
                moveq   #$1F,d0

loc_0_C1CC:                             ; CODE XREF: ROM:0000C1CEj
                move.l  d1,(a1)
                dbf     d0,loc_0_C1CC
                move.l  #$40000010,(a0)
                moveq   #$13,d0

loc_0_C1DA:                             ; CODE XREF: ROM:0000C1DCj
                move.l  d1,(a1)
                dbf     d0,loc_0_C1DA
                rts
; ---------------------------------------------------------------------------

set_colors:                             ; CODE XREF: ROM:0000C156p
                move.l  #-$40000000,($C00004).l
                move.l  #$8000EEE,($C00000).l
                move.l  #-$3FDE0000,($C00004).l
                move.w  #$E0,($C00000).l ; 'р'
                move.l  #-$3FBE0000,($C00004).l
                move.w  #$EE,($C00000).l ; 'ю'
                move.l  #-$3F9E0000,($C00004).l
                move.w  #$26E,($C00000).l
                rts
; ---------------------------------------------------------------------------

loc_0_C22E:                             ; CODE XREF: ROM:0000C15Ap
                move.l  #$40200000,($C00004).l
                move.w  #$2F7,d0
                slea0     font,a0
                lea     ($C00000).l,a1

loc_0_C246:                             ; CODE XREF: ROM:0000C25Cj
                move.b  (a0)+,d1
                moveq   #7,d2
                moveq   #0,d3

loc_0_C24C:                             ; CODE XREF: ROM:loc_0_C256j
                lsl.l   #4,d3
                btst    d2,d1
                beq.s   loc_0_C256
                bset    #0,d3

loc_0_C256:                             ; CODE XREF: ROM:0000C250j
                dbf     d2,loc_0_C24C
                move.l  d3,(a1)
                dbf     d0,loc_0_C246
                rts
; ---------------------------------------------------------------------------
font:           dc.w $3030              ; DATA XREF: ROM:0000C23Co
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b $30 ; 0
                dc.b   0
                dc.b $6C ; l
                dc.b $6C ; l
                dc.b $6C ; l
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $6C ; l
                dc.b $6C ; l
                dc.b $FE ; ¦
                dc.b $6C ; l
                dc.b $FE ; ¦
                dc.b $6C ; l
                dc.b $6C ; l
                dc.b   0
                dc.b $30 ; 0
                dc.b $7C ; |
                dc.b $C0 ; L
                dc.b $78 ; x
                dc.b  $C
                dc.b $F8 ; °
                dc.b $30 ; 0
                dc.b   0
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $CC ; ¦
                dc.b $18
                dc.b $30 ; 0
                dc.b $66 ; f
                dc.b $C6 ; ¦
                dc.b   0
                dc.b $38 ; 8
                dc.b $6C ; l
                dc.b $38 ; 8
                dc.b $76 ; v
                dc.b $DC ; -
                dc.b $CC ; ¦
                dc.b $76 ; v
                dc.b   0
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $C0 ; L
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $30 ; 0
                dc.b $18
                dc.b   0
                dc.b $60 ; `
                dc.b $30 ; 0
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b   0
                dc.b   0
                dc.b $66 ; f
                dc.b $3C ; <
                dc.b $FF
                dc.b $3C ; <
                dc.b $66 ; f
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $FC ; №
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $FC ; №
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b   6
                dc.b  $C
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b $C0 ; L
                dc.b $80 ; А
                dc.b   0
                dc.b $7C ; |
                dc.b $C6 ; ¦
                dc.b $CE ; +
                dc.b $DE ; ¦
                dc.b $F6 ; Ў
                dc.b $E6 ; ц
                dc.b $7C ; |
                dc.b   0
                dc.b $30 ; 0
                dc.b $70 ; p
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $78 ; x
                dc.b   0
                dc.b $F8 ; °
                dc.b  $C
                dc.b  $C
                dc.b $38 ; 8
                dc.b $60 ; `
                dc.b $C0 ; L
                dc.b $FC ; №
                dc.b   0
                dc.b $F8 ; °
                dc.b  $C
                dc.b  $C
                dc.b $38 ; 8
                dc.b  $C
                dc.b  $C
                dc.b $F8 ; °
                dc.b   0
                dc.b $1C
                dc.b $3C ; <
                dc.b $6C ; l
                dc.b $CC ; ¦
                dc.b $FE ; ¦
                dc.b  $C
                dc.b  $C
                dc.b   0
                dc.b $FC ; №
                dc.b $C0 ; L
                dc.b $F8 ; °
                dc.b  $C
                dc.b  $C
                dc.b  $C
                dc.b $F8 ; °
                dc.b   0
                dc.b $3C ; <
                dc.b $60 ; `
                dc.b $C0 ; L
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b   0
                dc.b $FC ; №
                dc.b  $C
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b   0
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b   0
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b  $C
                dc.b  $C
                dc.b $78 ; x
                dc.b   0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b $C0 ; L
                dc.b $60 ; `
                dc.b $30 ; 0
                dc.b $18
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $FC ; №
                dc.b   0
                dc.b   0
                dc.b $FC ; №
                dc.b   0
                dc.b   0
                dc.b $60 ; `
                dc.b $30 ; 0
                dc.b $18
                dc.b  $C
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b   0
                dc.b $F8 ; °
                dc.b  $C
                dc.b  $C
                dc.b $38 ; 8
                dc.b $30 ; 0
                dc.b   0
                dc.b $30 ; 0
                dc.b   0
                dc.b $7C ; |
                dc.b $C6 ; ¦
                dc.b $DE ; ¦
                dc.b $DE ; ¦
                dc.b $DE ; ¦
                dc.b $C0 ; L
                dc.b $78 ; x
                dc.b   0
                dc.b $30 ; 0
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $FC ; №
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b   0
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $F8 ; °
                dc.b   0
                dc.b $7C ; |
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $7C ; |
                dc.b   0
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $F8 ; °
                dc.b   0
                dc.b $FC ; №
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $F8 ; °
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $FC ; №
                dc.b   0
                dc.b $FC ; №
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $F8 ; °
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b   0
                dc.b $7C ; |
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $DC ; -
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b   0
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $FC ; №
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b   0
                dc.b $78 ; x
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $78 ; x
                dc.b   0
                dc.b $1E
                dc.b  $C
                dc.b  $C
                dc.b  $C
                dc.b  $C
                dc.b  $C
                dc.b $F8 ; °
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $CC ; ¦
                dc.b $D8 ; +
                dc.b $F0 ; Ё
                dc.b $D8 ; +
                dc.b $CC ; ¦
                dc.b $C6 ; ¦
                dc.b   0
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $FC ; №
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $EE ; ю
                dc.b $FE ; ¦
                dc.b $D6 ; г
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $E6 ; ц
                dc.b $F6 ; Ў
                dc.b $DE ; ¦
                dc.b $CE ; +
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b   0
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b   0
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $F8 ; °
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b   0
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $D8 ; +
                dc.b $6C ; l
                dc.b   0
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $F8 ; °
                dc.b $F0 ; Ё
                dc.b $D8 ; +
                dc.b $CC ; ¦
                dc.b   0
                dc.b $7C ; |
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $78 ; x
                dc.b  $C
                dc.b  $C
                dc.b $F8 ; °
                dc.b   0
                dc.b $FC ; №
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b   0
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b $30 ; 0
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b $D6 ; г
                dc.b $FE ; ¦
                dc.b $EE ; ю
                dc.b $C6 ; ¦
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b $6C ; l
                dc.b $38 ; 8
                dc.b $6C ; l
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b   0
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b $FE ; ¦
                dc.b  $C
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b $C0 ; L
                dc.b $FE ; ¦
                dc.b   0
                dc.b $78 ; x
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $78 ; x
                dc.b   0
                dc.b $C0 ; L
                dc.b $60 ; `
                dc.b $30 ; 0
                dc.b $18
                dc.b  $C
                dc.b   6
                dc.b   2
                dc.b   0
                dc.b $78 ; x
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b $78 ; x
                dc.b   0
                dc.b $10
                dc.b $38 ; 8
                dc.b $6C ; l
                dc.b $C6 ; ¦
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $FF
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $18
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $F8 ; °
                dc.b  $C
                dc.b $7C ; |
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b   0
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $F8 ; °
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $7C ; |
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $7C ; |
                dc.b   0
                dc.b  $C
                dc.b  $C
                dc.b $7C ; |
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $FC ; №
                dc.b $C0 ; L
                dc.b $7C ; |
                dc.b   0
                dc.b $3C ; <
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $F8 ; °
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $7C ; |
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b  $C
                dc.b $F8 ; °
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b   0
                dc.b $30 ; 0
                dc.b   0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b $18
                dc.b   0
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b $F0 ; Ё
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $CC ; ¦
                dc.b $D8 ; +
                dc.b $F0 ; Ё
                dc.b $D8 ; +
                dc.b $CC ; ¦
                dc.b   0
                dc.b $70 ; p
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $FE ; ¦
                dc.b $D6 ; г
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $78 ; x
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $F8 ; °
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $F8 ; °
                dc.b $C0 ; L
                dc.b   0
                dc.b   0
                dc.b $7C ; |
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b  $C
                dc.b   0
                dc.b   0
                dc.b $DC ; -
                dc.b $E0 ; р
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b $C0 ; L
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $7C ; |
                dc.b $C0 ; L
                dc.b $78 ; x
                dc.b  $C
                dc.b $F8 ; °
                dc.b   0
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $FC ; №
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $60 ; `
                dc.b $3C ; <
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $78 ; x
                dc.b $30 ; 0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $C6 ; ¦
                dc.b $D6 ; г
                dc.b $FE ; ¦
                dc.b $C6 ; ¦
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $C6 ; ¦
                dc.b $6C ; l
                dc.b $38 ; 8
                dc.b $6C ; l
                dc.b $C6 ; ¦
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $CC ; ¦
                dc.b $7C ; |
                dc.b  $C
                dc.b $F8 ; °
                dc.b   0
                dc.b   0
                dc.b $FC ; №
                dc.b $18
                dc.b $30 ; 0
                dc.b $60 ; `
                dc.b $FC ; №
                dc.b   0
                dc.b $1C
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $E0 ; р
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $1C
                dc.b   0
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b   0
                dc.b $18
                dc.b $18
                dc.b $18
                dc.b   0
                dc.b $E0 ; р
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $1C
                dc.b $30 ; 0
                dc.b $30 ; 0
                dc.b $E0 ; р
                dc.b   0
                dc.b $76 ; v
                dc.b $DC ; -
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
                dc.b   0
Text_C55A:      dc.b   0                ; DATA XREF: ROM:0000BE0Co
                dc.b $D4 ; L
                dc.b   0
                dc.b $10
                dc.b   0
                dc.b $1A
                dc.b   0
                dc.b $D4 ; L
                dc.b   0
                dc.b $D4 ; L
                dc.b   0
                dc.b $27 ; '
                dc.b   0
                dc.b $37 ; 7
                dc.b   0
                dc.b $4A ; J
                dc.b $55 ; U
                dc.b $73 ; s
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $20
                dc.b $44 ; D
                dc.b $61 ; a
                dc.b $74 ; t
                dc.b $61 ; a
                dc.b   0
                dc.b $55 ; U
                dc.b $73 ; s
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $20
                dc.b $50 ; P
                dc.b $72 ; r
                dc.b $6F ; o
                dc.b $67 ; g
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $6D ; m
                dc.b   0
                dc.b $53 ; S
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $76 ; v
                dc.b $69 ; i
                dc.b $73 ; s
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b $20
                dc.b $44 ; D
                dc.b $61 ; a
                dc.b $74 ; t
                dc.b $61 ; a
                dc.b   0
                dc.b $53 ; S
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $76 ; v
                dc.b $69 ; i
                dc.b $73 ; s
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b $20
                dc.b $50 ; P
                dc.b $72 ; r
                dc.b $6F ; o
                dc.b $67 ; g
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $6D ; m
                dc.b   0
                dc.b $43 ; C
                dc.b $50 ; P
                dc.b $55 ; U
                dc.b $20
                dc.b $53 ; S
                dc.b $70 ; p
                dc.b $61 ; a
                dc.b $63 ; c
                dc.b $65 ; e
                dc.b   0
word_0_C5AE:    dc.w $97                ; DATA XREF: ROM:0000BDC2o
                dc.b   0
                dc.b $9D ; Э
                dc.b   0
                dc.b $A3 ; г
                dc.b   0
                dc.b $AD ; н
                dc.b   0
                dc.b $BB ; ¬
                dc.b   0
                dc.b $CF ; ¦
                dc.b   0
                dc.b $DB ; -
                dc.b   0
                dc.b $EB ; ы
                dc.b   0
                dc.b $FD ; ¤
                dc.b   1
                dc.b $11
                dc.b   1
                dc.b $17
                dc.b   1
                dc.b $26 ; &
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   1
                dc.b $35 ; 5
                dc.b   1
                dc.b $42 ; B
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   1
                dc.b $61 ; a
                dc.b   1
                dc.b $74 ; t
                dc.b   1
                dc.b $86 ; Ж
                dc.b   1
                dc.b $98 ; Ш
                dc.b   1
                dc.b $AA ; к
                dc.b   1
                dc.b $BC ; -
                dc.b   1
                dc.b $CE ; +
                dc.b   1
                dc.b $E0 ; р
                dc.b   1
                dc.b $F2 ; Є
                dc.b   2
                dc.b   5
                dc.b   2
                dc.b $18
                dc.b   2
                dc.b $2B ; +
                dc.b   2
                dc.b $3E ; >
                dc.b   2
                dc.b $51 ; Q
                dc.b   2
                dc.b $64 ; d
                dc.b   2
                dc.b $77 ; w
                dc.b   2
                dc.b $8A ; К
                dc.b   2
                dc.b $9D ; Э
                dc.b   2
                dc.b $B0 ; -
                dc.b   2
                dc.b $C4 ; -
                dc.b   2
                dc.b $D8 ; +
                dc.b   2
                dc.b $EC ; ь
                dc.b   3
                dc.b   0
                dc.b   3
                dc.b $14
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b   0
                dc.b $80 ; А
                dc.b $28 ; (
                dc.b $55 ; U
                dc.b $6E ; n
                dc.b $61 ; a
                dc.b $73 ; s
                dc.b $73 ; s
                dc.b $69 ; i
                dc.b $67 ; g
                dc.b $6E ; n
                dc.b $65 ; e
                dc.b $64 ; d
                dc.b $2C ; ,
                dc.b $20
                dc.b $52 ; R
                dc.b $65 ; e
                dc.b $73 ; s
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $64 ; d
                dc.b $29 ; )
                dc.b   0
                dc.b $52 ; R
                dc.b $65 ; e
                dc.b $73 ; s
                dc.b $65 ; e
                dc.b $74 ; t
                dc.b   0
                dc.b $52 ; R
                dc.b $65 ; e
                dc.b $73 ; s
                dc.b $65 ; e
                dc.b $74 ; t
                dc.b   0
                dc.b $42 ; B
                dc.b $75 ; u
                dc.b $73 ; s
                dc.b $20
                dc.b $45 ; E
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b   0
                dc.b $41 ; A
                dc.b $64 ; d
                dc.b $64 ; d
                dc.b $72 ; r
                dc.b $65 ; e
                dc.b $73 ; s
                dc.b $73 ; s
                dc.b $20
                dc.b $45 ; E
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b   0
                dc.b $49 ; I
                dc.b $6C ; l
                dc.b $6C ; l
                dc.b $65 ; e
                dc.b $67 ; g
                dc.b $61 ; a
                dc.b $6C ; l
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $5A ; Z
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $6F ; o
                dc.b $20
                dc.b $44 ; D
                dc.b $69 ; i
                dc.b $76 ; v
                dc.b $69 ; i
                dc.b $64 ; d
                dc.b $65 ; e
                dc.b   0
                dc.b $43 ; C
                dc.b $48 ; H
                dc.b $4B ; K
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $52 ; R
                dc.b $41 ; A
                dc.b $50 ; P
                dc.b $56 ; V
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $50 ; P
                dc.b $72 ; r
                dc.b $69 ; i
                dc.b $76 ; v
                dc.b $69 ; i
                dc.b $6C ; l
                dc.b $65 ; e
                dc.b $67 ; g
                dc.b $65 ; e
                dc.b $20
                dc.b $56 ; V
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6C ; l
                dc.b $61 ; a
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $63 ; c
                dc.b $65 ; e
                dc.b   0
                dc.b $4C ; L
                dc.b $69 ; i
                dc.b $6E ; n
                dc.b $65 ; e
                dc.b $41 ; A
                dc.b $20
                dc.b $45 ; E
                dc.b $6D ; m
                dc.b $75 ; u
                dc.b $6C ; l
                dc.b $61 ; a
                dc.b $74 ; t
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b   0
                dc.b $4C ; L
                dc.b $69 ; i
                dc.b $6E ; n
                dc.b $65 ; e
                dc.b $46 ; F
                dc.b $20
                dc.b $45 ; E
                dc.b $6D ; m
                dc.b $75 ; u
                dc.b $6C ; l
                dc.b $61 ; a
                dc.b $74 ; t
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b   0
                dc.b $46 ; F
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b $6D ; m
                dc.b $61 ; a
                dc.b $74 ; t
                dc.b $20
                dc.b $45 ; E
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b   0
                dc.b $55 ; U
                dc.b $6E ; n
                dc.b $69 ; i
                dc.b $6E ; n
                dc.b $69 ; i
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $61 ; a
                dc.b $6C ; l
                dc.b $69 ; i
                dc.b $7A ; z
                dc.b $65 ; e
                dc.b $64 ; d
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b $20
                dc.b $56 ; V
                dc.b $65 ; e
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $6F ; o
                dc.b $72 ; r
                dc.b   0
                dc.b $53 ; S
                dc.b $70 ; p
                dc.b $75 ; u
                dc.b $72 ; r
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $75 ; u
                dc.b $73 ; s
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $4C ; L
                dc.b $65 ; e
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $6C ; l
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $4C ; L
                dc.b $65 ; e
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $6C ; l
                dc.b $2D ; -
                dc.b $32 ; 2
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $4C ; L
                dc.b $65 ; e
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $6C ; l
                dc.b $2D ; -
                dc.b $33 ; 3
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $4C ; L
                dc.b $65 ; e
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $6C ; l
                dc.b $2D ; -
                dc.b $34 ; 4
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $4C ; L
                dc.b $65 ; e
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $6C ; l
                dc.b $2D ; -
                dc.b $35 ; 5
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $4C ; L
                dc.b $65 ; e
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $6C ; l
                dc.b $2D ; -
                dc.b $36 ; 6
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $4C ; L
                dc.b $65 ; e
                dc.b $76 ; v
                dc.b $65 ; e
                dc.b $6C ; l
                dc.b $2D ; -
                dc.b $37 ; 7
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $74 ; t
                dc.b $65 ; e
                dc.b $72 ; r
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $70 ; p
                dc.b $74 ; t
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $30 ; 0
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $32 ; 2
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $33 ; 3
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $34 ; 4
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $35 ; 5
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $36 ; 6
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $37 ; 7
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $38 ; 8
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $39 ; 9
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $30 ; 0
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $31 ; 1
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $32 ; 2
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $33 ; 3
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $34 ; 4
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $54 ; T
                dc.b $72 ; r
                dc.b $61 ; a
                dc.b $70 ; p
                dc.b $2D ; -
                dc.b $31 ; 1
                dc.b $35 ; 5
                dc.b $20
                dc.b $49 ; I
                dc.b $6E ; n
                dc.b $73 ; s
                dc.b $74 ; t
                dc.b $72 ; r
                dc.b $75 ; u
                dc.b $63 ; c
                dc.b $74 ; t
                dc.b $69 ; i
                dc.b $6F ; o
                dc.b $6E ; n
                dc.b   0
                dc.b $48 ; H
                dc.b $E7 ; ч
                dc.b   0
                dc.b $C0 ; L
                dc.b $20
                dc.b $6F ; o
                dc.b   0
                dc.b  $C
                dc.b $22 ; "
                dc.b $6F ; o
                dc.b   0
                dc.b $10
                dc.b $61 ; a
                dc.b   6
                dc.b $4C ; L
                dc.b $DF ; -
                dc.b   3
                dc.b   0
                dc.b $4E ; N
                dc.b $75 ; u
                dc.b $48 ; H
                dc.b $E7 ; ч
                dc.b $FF
                dc.b $FE ; ¦
                dc.b $32 ; 2
                dc.b $18
                dc.b $30 ; 0
                dc.b $18
