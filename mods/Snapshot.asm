;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

DebugVar macro arg,adr
    dc.l arg*$1000000+adr
    endm

; ---------------------------------------------------------------------------
; Insert debug
; ---------------------------------------------------------------------------

	org $8

		DebugVar	2,Debug;		Slot #2 : ошибка шины
		DebugVar	3,Debug;		Slot #3 : ошибка адресации
		DebugVar	4,Debug;		Slot #4 : неизвестная операция
		DebugVar	5,Debug;		Slot #5 : деление на 0
		DebugVar	6,Debug;		Slot #6 : команда СHK
		DebugVar	7,Debug;		Slot #7 : команда TRAPV
		DebugVar	8,Debug;		Slot #8 : нарушение привилегий
		DebugVar	9,Debug;		Slot #9 : трассировка
		DebugVar	10,Debug;		Slot #A : Зарезервированы (не используются)
		DebugVar	11,Debug;RootNumber;		Slot #B :
		DebugVar	12,Debug;		Slot #C :
		DebugVar	13,Debug;		Slot #D :
		DebugVar	14,Debug;		Slot #E :
		DebugVar	15,Debug;		Slot #F : неинициализированный вектор прерывания
		DebugVar	16,Debug;		Slot #10: Зарезервированы (не используются)
		DebugVar	17,Debug;		Slot #11:
		DebugVar	18,Debug;		Slot #12:
		DebugVar	19,Debug;		Slot #13:
		DebugVar	20,Debug;		Slot #14:
		DebugVar	21,Debug;		Slot #15:
		DebugVar	22,Debug;		Slot #16:
		DebugVar	23,Debug;		Slot #17:

; ---------------------------------------------------------------------------
; Load snapshot
; ---------------------------------------------------------------------------

	org $20E

	bne.s $28E

	org $288

	jmp   fifif;
	bra   $2FA;

; ---------------------------------------------------------------------------
; Turn off red screen
; ---------------------------------------------------------------------------

	;org $306

	;rte

; ---------------------------------------------------------------------------
; Turn off timeout
; ---------------------------------------------------------------------------

	org $6DA

	bra $3C6

; ---------------------------------------------------------------------------
; Increase max life
; ---------------------------------------------------------------------------

	org $17032

	dc.w $60

	org $17054

	dc.w $60

; ---------------------------------------------------------------------------
; Hotfixes
; ---------------------------------------------------------------------------

	org $C020
	jmp Ho1tFix1

	org $111AC
	jmp HotFix2

	org $16C1E
	jmp HotFix3

	org $d96c

	jmp FixPrintText

	org $16CE8

	jmp FixCounter

	org $12AC4

	jmp EnableWordAmmo

; ---------------------------------------------------------------------------
; Fix Timer Beeping
; ---------------------------------------------------------------------------

	org $105D0

	bhi.s $105DA

; ---------------------------------------------------------------------------
; Fix Weapon Glitch - Unable to shoot
; ---------------------------------------------------------------------------

	org $1F3AC

	beq.w $1CEBE

	org $1F99C

	beq.w $1F3D4

	org $1F6E4

	beq.w $1F3D4

	org $1F8B4

	beq.w $1F3D4

	org $1F76A

	beq.s $1F7C0

	org $1F842

	beq.s $1F89A

	org $1EDF4

	beq.w $20204

; ---------------------------------------------------------------------------
; Allow Run'n'Gun Always
; ---------------------------------------------------------------------------

	;org $20F9E
	;bra.s $20FB8
	;org $20C7A
	;bra.s $20C94

	;org $1EB74
	;nop
	;nop

; ---------------------------------------------------------------------------


 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; debug
; ---------------------------------------------------------------------------
sjsr macro adress
        dc.w $4EBA,adress-*-2
        endm
slea0 macro adress,align
        dc.w $41FA,adress-*-2
        endm
slea1 macro adress,align
        dc.w $43FA,adress-*-2
        endm
slea2 macro adress,align
        dc.w $45FA,adress-*-2
        endm
slea3 macro adress,align
        dc.w $47FA,adress-*-2
        endm
slea4 macro adress,align
        dc.w $49FA,adress-*-2
        endm
slea5 macro adress,align
        dc.w $4BFA,adress-*-2
        endm
slea6 macro adress,align
        dc.w $4DFA,adress-*-2
        endm
slea7 macro adress,align
        dc.w $4FFA,adress-*-2
        endm


	include "mods\Debug.asm"

; ---------------------------------------------------------------------------
; fifif
; ---------------------------------------------------------------------------

fifif:
	move  #$2700,sr
	;rts
	;jsr   LoadSnapshot

LoadSnapshot:
	lea    binSnapshot,a4
	lea    ($FFFF0000).l,a3
	move.w #$7FFF,d0
loop:
	move.w (a4)+,(a3)+
	dbra   d0,loop

	move  #$2700,sr
	jmp   $2FA;

;LoadSnapshot:
;	lea    binSnapshot,a4
;	lea    ($FFFF0000).l,a3
;	move.w #$7FFF,d0
;loop:
;	move.w (a4)+,(a3)+
;	dbra   d0,loop
;	rts

binSnapshot:
	binclude "mods\dump.bin"

	align 2

Ho1tFix1:
	add.w    d0,d0
	adda.w   (a0,d0.w),a0
	move.l   a0,d0
	and.w    #$FFFE,d0
	move.l   d0,a0
	jmp      $C026


HotFix2:
	move.l   a0,d0
	and.w    #$FFFE,d0
	move.l   d0,a0
	lea      $5BE4A,a1
	jmp		 $111B2

HotFix3:
	add.w    d0,d0
	adda.w   (a0,d0.w),a0
	move.l   a0,d0
	and.w    #$FFFE,d0
	move.l   d0,a0
    jmp      $16C24

FixPrintText:
    cmp.b    #$D,d0
    bne      FixedReturn
    jmp      $D972

FixedReturn:
	jmp      $D94A


FixCounter:
    lea		$16884,a0
    lea     $20(a6),a1
    abcd	-(a0),-(a1)
    abcd	-(a0),-(a1)
    jmp		$16CF4

EnableWordAmmo:
	move.w  $4C(a6,d0.w),d0
    lsr.w   #8,d0
	rol.b   #4,d0
	moveq   #$F,d1
	and.w   d0,d1
	add.w   d1,d1
	addi.w  #$E5DA,d1
	move.w  #$140,(a1)+
	move.b  #1,(a1)+
	move.b  d5,(a1)+
	addq.w  #1,d5
	move.w  d1,(a1)+
	move.w  #$B8,(a1)+ ; '¬'
	rol.b   #4,d0
	moveq   #$F,d1
	and.w   d0,d1
	add.w   d1,d1
	addi.w  #$E5DA,d1
	move.w  #$140,(a1)+
	move.b  #1,(a1)+
	move.b  d5,(a1)+
	addq.w  #1,d5
	move.w  d1,(a1)+
	move.w  #$C0,(a1)+ ; 'L'

	move.w  $90(a6),d0
    add.w   d0,d0
    move.w  $4C(a6,d0.w),d0
	rol.b   #4,d0
	moveq   #$F,d1
	and.w   d0,d1
	add.w   d1,d1
	addi.w  #$E5DA,d1
	move.w  #$140,(a1)+
	move.b  #1,(a1)+
	move.b  d5,(a1)+
	addq.w  #1,d5
	move.w  d1,(a1)+
	move.w  #$C8,(a1)+ ; '¬'
	rol.b   #4,d0
	moveq   #$F,d1
	and.w   d0,d1
	add.w   d1,d1
	addi.w  #$E5DA,d1
	move.w  #$140,(a1)+
	move.b  #1,(a1)+
	move.b  d5,(a1)+
	addq.w  #1,d5
	move.w  d1,(a1)+
	move.w  #$D0,(a1)+ ; 'L'

	jmp		$12B04

 endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
